Rails.application.routes.draw do
  root to: "sessions#new"

  resource :session, only: [:create]

  resources :characters, only: [:index, :show]

  namespace :api do
    resources :characters, only: [:index, :show, :create, :update, :destroy]
  end
end
