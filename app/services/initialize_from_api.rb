class InitializeFromApi
  attr_accessor :public_key, :private_key, :marvel_client
  CHARACTERS_PER_REQUEST = 100
  COMICS_PER_REQUEST = 100

  def initialize(public_key:, private_key:)
    @public_key  =  public_key
    @private_key =  private_key
  end

  def get_characters
    response = marvel_client.characters until response.present?

    characters_count = response['data']['total'].to_f

    number_of_requests = (characters_count / CHARACTERS_PER_REQUEST).ceil

    (1..number_of_requests).each do |n|
      offset = (n-1) * CHARACTERS_PER_REQUEST

      characters = marvel_client.characters(limit: CHARACTERS_PER_REQUEST, offset: offset)

      unless characters.present?
        # sometimes the api can fail, so we need to redo the loop when it happens
        redo
      end

      create_characters(characters['data']['results'])
    end
  end

  def get_comics(marvel_id)
    comics = []

    response = marvel_client.character_comics(marvel_id) until response.present?

    comics_count = response['data']['total'].to_f

    number_of_requests = (comics_count / COMICS_PER_REQUEST).ceil

    (1..number_of_requests).each do |n|
      offset = (n-1) * COMICS_PER_REQUEST

      api_comics = marvel_client.character_comics(marvel_id, limit: CHARACTERS_PER_REQUEST, offset: offset)

      unless api_comics.present?
        # sometimes the api can fail, so we need to redo the loop when it happens
        redo
      end

      api_comics['data']['results'].each do |api_comic|
        comics << Comic.new(title: api_comic['title'], description: api_comic['description'], issue_number: api_comic['issueNumber'], thumbnail: thumbnail(api_comic))
      end
    end
    comics
  end

  def create_characters(characters)
    characters.each do |character|
      Character.find_or_create_by(
        marvel_id: character['id'],
        name: character['name'],
        description: character['description'],
        modified: character['modified'],
        thumbnail: thumbnail(character),
        )
    end
  end

  private
    def marvel_client
      @marvel_client ||= Marvelite::API::Client.new(public_key: public_key, private_key: private_key)
    end

    def thumbnail(object)
      "#{object['thumbnail']['path']}/portrait_xlarge.#{object['thumbnail']['extension']}"
    end
end
