class Character < ApplicationRecord
  validates :marvel_id, uniqueness: true
  self.per_page = 20
end
