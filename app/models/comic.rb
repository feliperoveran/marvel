class Comic
  attr_reader :title, :description, :thumbnail, :issue_number

  def initialize(title:, description:, issue_number:, thumbnail:)
    @title, @description, @issue_number, @thumbnail = title, description, issue_number, thumbnail
  end
end
