class SessionsController < ApplicationController
  # before_action :load_schema, only: [:create]
  before_action :validate_keys, only: [:create]

  def new

  end

  def create
    private_key = session_params[:private_key]
    public_key = session_params[:public_key]

    store_keys

    CreateCharactersJob.perform_later(private_key: private_key, public_key: public_key)

    redirect_to characters_path, flash: { success: 'As informações estão sendo carregadas. Por favor, aguarde alguns instantes e recarregue a página para ver os resultados' }
  end

  private
    def load_schema
      # As we are using an in-memory db, we need to load the schema every time our app runs
      load "#{Rails.root}/db/schema.rb"
    end

    def session_params
      params.fetch(:session, {}).permit(:private_key, :public_key)
    end

    def store_keys
      # using cookie storage
      cookies.permanent.encrypted[:private_key] = session_params[:private_key]
      cookies.permanent.encrypted[:public_key] = session_params[:public_key]
    end

    def validate_keys
      # do a test api request to see if the keys are valid
      response = Marvelite::API::Client.new(public_key: session_params[:public_key], private_key: session_params[:private_key]).characters

      raise InvalidCredentialsError if response.has_value?('InvalidCredentials')
    end
end
