class Api::CharactersController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  skip_before_action :verify_authenticity_token
  before_action :set_character, only: [:show, :edit, :update, :destroy]

  # GET /api/characters
  # GET /api/characters.json
  def index
    @characters = Character.paginate(page: params[:page])

    render json: {
      current_page: @characters.current_page,
      per_page: @characters.per_page,
      total: @characters.total_entries,
      characters: @characters
    }
  end

  # GET /api/characters/1
  # GET /api/characters/1.json
  def show
    render json: @character
  end

  # POST /api/characters
  # POST /api/characters.json
  def create
    @character = Character.new(character_params)

    if @character.save
      render json: @character, status: :created
    else
      render json: @character.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/characters/1
  # PATCH/PUT /api/characters/1.json
  def update
    if @character.update(character_params)
      render json: @character, status: :ok
    else
      render json: @character.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/characters/1
  # DELETE /api/characters/1.json
  def destroy
    if @character.destroy
      render json: { message: 'Character deleted.' }, status: :ok
    else
      render json: @character.errors, status: :unprocessable_entity
    end
  end

  private
    def set_character
      @character = Character.find(params[:id])
    end

    def character_params
      params.fetch(:character, {}).permit(:marvel_id, :name, :description, :modified, :thumbnail)
    end

    def record_not_found
      render json: { error: "Character wasn't found" }, status: :not_found
    end
end
