class CharactersController < ApplicationController
  def index
    @characters = Character.paginate(page: params[:page])
  end

  def show
    @character = Character.find(params[:id])

    @comics = InitializeFromApi.new(private_key: cookies.encrypted[:private_key], public_key: cookies.encrypted[:public_key]).get_comics(@character.marvel_id)
  end
end
