class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  # if for some reason the database dies, we catch the exception and redirect to the home page with a flash message
  rescue_from 'ActiveRecord::StatementInvalid' do
    redirect_to root_path, flash: { danger: 'Ocorreu um erro. Por favor entre novamente.' }
  end

  rescue_from 'InvalidCredentialsError' do
    redirect_to root_path, flash: { danger: 'As chaves fornecidas são inválidas. Por favor verifique e tente novamente.' }
  end
end
