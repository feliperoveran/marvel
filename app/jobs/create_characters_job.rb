class CreateCharactersJob < ApplicationJob
  queue_as :default

  def perform(private_key:, public_key:)
    InitializeFromApi.new(public_key: public_key, private_key: private_key).get_characters
  end
end
