class CreateCharacters < ActiveRecord::Migration[5.0]
  def change
    create_table :characters do |t|
      t.integer :marvel_id
      t.text :name
      t.text :description
      t.datetime :modified

      t.timestamps
    end
  end
end
