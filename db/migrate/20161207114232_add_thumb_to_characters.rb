class AddThumbToCharacters < ActiveRecord::Migration[5.0]
  def change
    add_column :characters, :thumbnail, :string
  end
end
